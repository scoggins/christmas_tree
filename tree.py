#!/usr/bin/env python

import sys, os, time, array, socket, random
import Queue, threading, logging

log = logging.getLogger("Tree")
logHandler = logging.StreamHandler()
logHandler.setFormatter(logging.Formatter("%(asctime)s %(threadName)s - %(levelname)s - %(message)s"))
log.addHandler(logHandler)
log.setLevel(logging.INFO)

class Lights:
	lights = [[0 , 0 , 0], ] * 50 # Make the light array
	IP = ''

	def __init__(self, IP):
		self.IP = IP

	def setAllLights(self, colour):
		for idx in range(0, len(self.lights)):
			self.lights[idx] = colour
		self.updateLights()

	def goDark(self):
		self.setAllLights([0, 0, 0])

	def changeColours(self, intensity=100, colours=2, mode="random"):
		mark = 0

		for idx in range(0, 50):
			x = 10 + int(random.randint(0, 245) / (100.00/intensity))
			if mode == "random":
				mark = random.randint(0, colours-1)

			if mark == 0:
				self.lights[idx] = [ x, x, x]
			elif mark == 1: 
				self.lights[idx] = [ 0, 0, x]
			elif mark == 2: 
				self.lights[idx] = [ 0, x, 0]
			elif mark == 3: 
				self.lights[idx] = [ x, 0, 0]

			if mode == "alternate":
				if mark <4:
					mark += 1
				else:
					mark = 0



		self.updateLights()

	def cycleColours(self):
		for cnt in range(0, 50):
			self.lights = self.lights[-1:] + self.lights[:-1]	
			self.updateLights()
			time.sleep(0.2)

	def colourPulse(self, RGB="red", count=2):
		for loop in range(0, count):
			aRange = range(0, 255)
			for x in aRange:
				if RGB == "red":
					colour = [x, 0, 0]
				elif RGB == "green":
					colour = [0, x, 0]
				elif RGB == "blue":
					colour = [0, 0, x]
				self.setAllLights(colour)
				time.sleep(0.001)
			aRange.reverse()
			for x in aRange:
				if RGB == "red":
					colour = [x, 0, 0]
				elif RGB == "green":
					colour = [0, x, 0]
				elif RGB == "blue":
					colour = [0, 0, x]
				self.setAllLights(colour)
				time.sleep(0.001)
			time.sleep(0.5)


	def updateLights(self):
		sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
		packet = array.array('B', [ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0])  # initialize basic packet, ignore first 10 bytes
		for light in self.lights:
			packet.append(light[0])
			packet.append(light[1])
			packet.append(light[2])

		# Send the packet to the Holiday
		sock.sendto(packet, (self.IP, 9988))
		return

def main():
	tree = Lights("10.10.10.145")
	action = "sleep"
	intensity = 100

	# Starting at Sunday.. 
	OperatingTimes = [
		[ "8:00:00", "21:30:00" ], # Monday
		[ "8:00:00", "21:30:00" ], # Tuesday
		[ "8:00:00", "21:30:00" ], # Wednesday
		[ "6:00:00", "21:30:00" ], # Thursday
		[ "8:00:00", "22:30:00" ], # Friday
		[ "8:00:00", "22:30:00" ], # Saturday
		[ "8:00:00", "21:30:00" ], # Sunday
	]
	try:
		while True:
			now = int(time.time())
			nowLocal = time.localtime(now)
			year = nowLocal[0]
			month = nowLocal[1]
			day = nowLocal[2]
			hour = nowLocal[3]
			minute = nowLocal[4]
			second = nowLocal[5]

			log.debug("%d:%d.%d %s %d" % (hour, minute, second, action, intensity))

			if action == "sleep":
				start = list(time.localtime(now))
				(hour, minute, second) = OperatingTimes[nowLocal[6]][0].split(":")
				start[3] = int(hour)
				start[4] = int(minute)
				start[5] = int(second)
				start = time.mktime(start)
				end = list(time.localtime(now))
				(hour, minute, second) = OperatingTimes[nowLocal[6]][1].split(":")
				end[3] = int(hour)
				end[4] = int(minute)
				end[5] = int(second)
				end = time.mktime(end)

				# Check if we should already be on
				if now > start and now < end:
					action = "normal"
				else:
					action = "sleep"


			elif action == "normal":

				(offHour, offMinute, offSecond) = OperatingTimes[nowLocal[6]][1].split(":")

				if hour >= int(offHour) and minute >= int(offMinute) and second >= int(offSecond):
					tree.colourPulse("red")
					tree.goDark()
					action = "sleep"
					continue

				if minute == 0 and second == 0:
					count = hour
					if count > 12:
						count=count-12
					tree.colourPulse("green", count)
				elif random.randint(0, 100) == 0:
					log.debug("Cycle the colours!")
					tree.cycleColours()
				elif now % 2 == 0:
					if month == 12 and day == 25:
						tree.changeColours(intensity, colours=4)
					else:
						tree.changeColours(intensity)

				if hour >= 19 and intensity > 30:
					intensity = 30
				elif hour < 19 and intensity <100:
					intensity = 100


			
			time.sleep(1)
	except:
		tree.goDark()


main()

